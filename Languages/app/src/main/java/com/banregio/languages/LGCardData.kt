package com.banregio.languages

data class LGCardData(val title: String, val description: String, val image: Int)
