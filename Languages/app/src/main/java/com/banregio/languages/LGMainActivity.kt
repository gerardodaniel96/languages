package com.banregio.languages

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ListView

class LGMainActivity : AppCompatActivity() {
    private lateinit var cardListView: ListView
    private var listOfCards = listOf<LGCardData>()

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_Languages)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setup()
    }

    private fun setup() {
        cardListView = findViewById(R.id.lvCards)
        createCards()
        createAdapter()
    }

    private fun createCards() {
        val card = LGCardData(getString(R.string.cardTitle), getString(R.string.cardDescription), R.drawable.platinum_card)
        val card2 = LGCardData(getString(R.string.card2Title), getString(R.string.card2Description), R.drawable.plus_card)
        val card3 = LGCardData(getString(R.string.card3Title), getString(R.string.card3Description), R.drawable.gold_card)
        val card4 = LGCardData(getString(R.string.card4Title), getString(R.string.card4Description), R.drawable.classic_card)
        val card5 = LGCardData(getString(R.string.card5Title), getString(R.string.card5Description), R.drawable.bussiness_card)
        listOfCards = listOf(card, card2, card3, card4, card5)
    }

    private fun createAdapter() {
        val cardsAdapter = LGCardAdapter(this, listOfCards)
        cardListView.adapter = cardsAdapter
    }
}