package com.banregio.languages

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView

class LGCardAdapter(private val cardContext: Context, private val cardsList: List<LGCardData>): ArrayAdapter<LGCardData>(cardContext, 0, cardsList) {
    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layout = LayoutInflater.from(cardContext).inflate(R.layout.card_view, parent, false)

        val card = cardsList[position]

        val cardImage: ImageView = layout.findViewById(R.id.ivCard)
        val cardTitle: TextView = layout.findViewById(R.id.tvCardTitle)
        val cardDescription: TextView = layout.findViewById(R.id.tvCardDescription)
        cardImage.setImageResource(card.image)
        cardTitle.text = card.title
        cardDescription.text = card.description

        return layout
    }
}